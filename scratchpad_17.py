# # # Y75HL?g>e3+Fr=ds,jM$KC

# # import paramiko

# # HOST = ""
# # USER = ""
# # PASSWORD = ""

# # ssh = paramiko.SSHClient()
# # ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# # ssh.connect(HOST, username=USER, password=PASSWORD)

# # stdin, stdout, stderr = ssh.exec_command("sudo apt update")
# # print(stdout.read().decode())

# # stdin, stdout, stderr = ssh.exec_command("sudo apt install -y apache2")
# # print(stdout.read().decode())

# # html = "<html><body><h1>Hello World From Python!</h1></body></html>"
# # stdin, stdout, stderr = ssh.exec_command(
# #     f"echo '{html}' | sudo tee /var/www/html/index.html"
# # )
# # print(stdout.read().decode())

# # stdin, stdout, stderr = ssh.exec_command("sudo service apache2 start")
# # print(stdout.read().decode())

# # ssh.close()

# import paramiko
# import time

# IP_ADDRESS = ""
# USERNAME = ""
# PASSWORD = ""

# ssh_client = paramiko.SSHClient()
# ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# ssh_client.connect(hostname=IP_ADDRESS, username=USERNAME, password=PASSWORD)

# print("Successful connection", IP_ADDRESS)

# remote_connection = ssh_client.invoke_shell()
# remote_connection.send("conf t\n")
# remote_connection.send("int loop 0\n")
# remote_connection.send("ip address 1.1.1.1 255.255.255.255\n")
# remote_connection.send("int loop 1\n")
# remote_connection.send("ip address 2.2.2.2 255.255.255.255\n")

# for n in range(2, 21):
#     print(f"Creating VLAN {n}")
#     remote_connection.send(f"vlan n\n")
#     remote_connection.send(f"name PYTHON_VLAN_{n}\n")
#     time.sleep(1)

#     output = remote_connection.recv(65535)
#     print(output.decode("ascii"))

#     ssh_client.close()

# import telnetlib

# HOST = ""
# USERNAME = ""
# PASSWORD = ""

# tn = telnetlib.Telnet(HOST)

# tn.read_until("Username:")
# tn.write(USERNAME.encode("ascii") + b"\n")
# tn.read_until("Password:")
# tn.write(PASSWORD.encode("ascii") + b"\n")

# tn.write("conf t\n")
# for n in range(2, 10):
#     tn.write(b"vlan " + str(n).encode("ascii") + b"\n")
#     tn.write(b"name PYTHON_VLAN_" + str(n).encode("ascii") + b"\n")


# tn.write(b"end\n")
# tn.write(b"exit\n")

# print(tn.read_all().decode("ascii"))

# from netmiko import ConnectHandler

# iosv = {"device_type": "cisco_ios", "ip": "", "username": "", "password": ""}

# net_connect = ConnectHandler(**iosv)

# output = net_connect.send_commant("show ip int br")
# print(output)

# configs = ["int loop 0", "ip address 1.1.1.1 255.255.255.0"]

# output = net_connect.send_config_set(configs)
# print(output)

# from pytube import YouTube

# video_url = input("Enter the YouTube video url: ")

# yt = YouTube(video_url)
# stream = yt.streams.get_highest_resolution()

# stream.download()

# print(f"Successfully downloaded video {yt.title} to the current directory.")
