# # with open("hello.txt") as file:
# #     print(file.read())

# # with open("hello.txt", "a") as file:
# #     file.write("This is some more content\nHello World!\n")

# # with open("hello.txt", "r+") as file:
# #     file.seek(0)
# #     file.write("HELLLLLLO!!!!")

# # with open("hello2.txt") as file:
# #     contents = file.read()

# # modified_content = contents.replace("World", "Universe")

# # with open("hello2.txt", "w") as file:
# #     file.write(modified_content)

# from pprint import pprint

# # with open("test.csv") as file:
# #     contents = file.read()
# #     rows = contents.split("\n")[0:-1]
# #     pprint([row.split(",") for row in rows])

# # from csv import reader, DictReader

# # with open("test.csv") as file:
# #     # csv_reader = reader(file)
# #     # pprint(list(csv_reader))

# #     csv_reader = DictReader(file, delimiter="|")
# #     pprint(list(csv_reader))


# # from csv import writer

# # with open("test.csv", "a", newline="") as file:
# #     csv_writer = writer(file)

# #     csv_writer.writerow(["Richard", "Roe", 27, "rich@hey.com"])
# #     csv_writer.writerow(["Richard", "Roe", 27, "rich@hey.com"])
# #     csv_writer.writerow(["Richard", "Roe", 27, "rich@hey.com"])
# #     csv_writer.writerow(["Richard", "Roe", 27, "rich@hey.com"])

# # from csv import DictWriter

# # with open("test.csv", "w", newline="") as file:
# #     headers = ["First Name", "Last Name", "Email"]
# #     csv_writer = DictWriter(file, fieldnames=headers)
# #     csv_writer.writeheader()
# #     csv_writer.writerow(
# #         {"First Name": "Richard", "Last Name": "Roe", "Email": "rich@hey.com"}
# #     )
# #     csv_writer.writerow(
# #         {"First Name": "Richard", "Last Name": "Roe", "Email": "rich@hey.com"}
# #     )
# #     csv_writer.writerow(
# #         {"First Name": "Richard", "Last Name": "Roe", "Email": "rich@hey.com"}
# #     )

# import pickle
# import json
# import jsonpickle


# class User:
#     def __init__(self, first_name, last_name, age):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age

#     def __repr__(self):
#         return f"{self.first_name} {self.last_name} | {self.age}"

#     def greet(self):
#         return f"Hello, I'm {self.first_name}"


# # user1 = User("John", "Doe", 20)
# # user2 = User("Jane", "Smith", 30)

# # with open("users.json", "w") as file:
# #     file.write(jsonpickle.encode((user1, user2)))

# with open("users.json") as file:
#     contents = file.read()
#     data = jsonpickle.decode(contents)
#     print(data)


# # with open("users.json", "w") as file:
# #     file.write(json.dumps((user1.__dict__, user2.__dict__)))

# # user2.age = 35


# # print(user1)
# # print(user1.greet())
# # print(user2)


# # with open("users.pickle", "wb") as file:
# #     pickle.dump((user1, user2), file)

# # with open("users.pickle", "rb") as file:
# #     data = pickle.load(file)
# #     print(data[0].greet())

# # JSON
# # "{"first_name": "John", "last_name": "Doe", "age": 20}"
# # XML
# # <users>
# #     <user>
# #         <first_name>John</first_name>
# #         <last_name>Doe</last_name>
# #     </user>
# #     <user>
# #         <first_name>John</first_name>
# #         <last_name>Doe</last_name>
# #     </user>
# #     <user>
# #         <first_name>John</first_name>
# #         <last_name>Doe</last_name>
# #     </user>
# #     <user>
# #         <first_name>John</first_name>
# #         <last_name>Doe</last_name>
# #     </user>
# # </users>

# # [
# #     {
# #         "first_name": "John",
# #         "last_name": "Doe"
# #     },
# #     {
# #         "first_name": "John",
# #         "last_name": "Doe"
# #     },
# #     {
# #         "first_name": "John",
# #         "last_name": "Doe"
# #     }
# # ]

# Bank
# Customer

# Bank (bank.py)
## $ Attributes
### name: str, initials: str, address: str, phone_nos: str[], ifsc_code: str, branch: str, email: str, customers: Customer[]
## @ Methods
### get_details, update_details, create_customer, find_customer/filter_customer, delete_customer, get_all_customers, export_customers_to_csv

# Customer (customer.py)
## Attributes
### first_name: str, middle_name: str, last_name: str, phone: str, address: str, email: str, aadhar_card_no: str, pan_card_no: str, account_no: str, balance: float
## @ Methods
### get_details, update_details, get_balance, deposit, withdraw

# main.py

# sbi = Bank(aksjndkjasnd)
# cus1 = sbi.create_customer(akjsdnkjasd)


class Company:
    def __init__(self):
        self.employees = []
        pass

    @classmethod
    def generate_account_no(cls):
        pass

    def create_employee(
        self, first_name, middle_name, last_name, aadhar_card_no, pan_card_no
    ):
        emp = Employee(first_name, middle_name, last_name)
        self.employees.append(emp)
        return emp


class Employee:
    def __init__(
        self,
        first_name,
        middle_name,
        last_name,
        address,
        aadhar_card_no,
        pan_card_no,
        phone,
        email,
        employee_id,
    ):
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.address = address
        self.aadhar_card_no = aadhar_card_no
        self.pan_card_no = pan_card_no
        self.phone = phone
        self.email = email
        self._employee_id = employee_id

    def __repr__(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"

    def get_full_name(self):
        return f"{self.first_name} {self.middle_name} {self.last_name}"

    def get_details(self):
        return {
            "name": self.get_full_name(),
            "address": self.address,
            "aadhar_card_no": self.aadhar_card_no,
            "pan_card_no": self.pan_card_no,
            "phone": self.phone,
            "email": self.email,
            "employee_id": self._employee_id,
        }

    def edit_details(
        self,
        first_name=None,
        middle_name=None,
        last_name=None,
        address=None,
        aadhar_card_no=None,
        pan_card_no=None,
        email=None,
        phone=None,
    ):
        if first_name:
            self.first_name = first_name
        if middle_name:
            self.middle_name = middle_name


# user1.edit_details(first_name="Jack", email="kjasndkasd@kjanfas.com")

# sbi = Bank()
# john = sbi.create_customer(njknsdkjfn)
