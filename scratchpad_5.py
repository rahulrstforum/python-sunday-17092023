product = [1, 'Pixel 8 Pro', 'Google', 'Some desc...', 500, 100000, True]

# product[1], product[2]

product = {
    "index": 1,
    "price": 100000,
    "name": "Pixel 8 Pro",
    "brand": "Google",
    "in_stock": 500,
    "description": "Some desc....",
    "discounted": True,
}

# for key in product:
#     print(key, product[key])

for k, v in product.items():
    print(k, v)
