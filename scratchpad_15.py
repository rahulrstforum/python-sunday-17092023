# import sqlite3
# from pprint import pprint

# conn = sqlite3.connect("mydb.sqlite")
# cursor = conn.cursor()

# # cursor.execute(
# #     "CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT, email TEXT, age INTEGER);"
# # )

# # user1 = ("John Doe", "john@gmail.com", 20)
# # users = [
# #     ("John Doe", "john@gmail.com", 20),
# #     ("Jane Smith", "janesmith@gmail.com", 22),
# #     ("Jack Roe", "jjack@outlook.com", 34),
# # ]

# # for user in users:
# #     print(f"Writing {user[0]} to database...")
# #     cursor.execute("INSERT INTO users (name, email, age) VALUES (?, ?, ?)", user)

# # cursor.execute("SELECT * FROM users")
# # all_users = cursor.fetchall()
# # pprint(all_users)

# # cursor.execute("SELECT * FROM users WHERE name = ?", ("John Doe",))
# # johns = cursor.fetchall()
# # print(johns)

# cursor.execute("SELECT * FROM users WHERE name LIKE '%it%'")
# result = cursor.fetchall()
# print(result)

# conn.commit()
# conn.close()

from pprint import pprint
import pymongo

client = pymongo.MongoClient()
mydb = client["pymongo_users"]
users = mydb["users"]

# user1 = {"name": "John Doe", "email": "john@example.com", "age": 20}

# users_data = [
#     {"name": "Jane Doe", "email": "jane@example.com", "age": 22},
#     {"name": "Jack Smith", "email": "jack.smith@microsoft.com", "age": 30},
# ]

# # users.insert_one(user1)
# users.insert_many(users_data)

# all_users = users.find()
# pprint(list(all_users))

# users.update_one(
#     {"name": "John Doe"},
#     {"$set": {"email": "john_new@example2.com", "phone": "+919876654321"}},
# )

# age_result = users.find({"age": {"$gte": 25, "$lte": 35}})

# for user in age_result:
#     print(user)

# users.delete_one({"age": 30})
