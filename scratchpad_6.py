# def greet():
#     print("Hello World")
#     print("Goodbye World")

# greet()
# greet()
# greet()

# from random import random

# def flip_coin():
#     num = random()
#     if num > 0.5:
#         print("HEADS")
#     else:
#         print("TAILS")

# for _ in range(3):
#     flip_coin()

# def greet():
#     # print("Hello World")
#     return "Hello World"

# result = greet()
# print(result)

# def greet(name: str, greeting: str) -> str:
#     return f"{greeting} {name}, how are you?"

# greet()

# print(greet("Jane", "Hi"))
# print(greet("John", "Hello"))
# print(greet("Jack", "Hey"))

# def add(a, b):
#     return a + b

# print(add(10, 4))

# def add(nums):
#     total = 0
#     for num in nums:
#         total += num
#     return total

# print(add([1, 2, 3, 2.3]))

# def sum_odd_nums(numbers):
#     total = 0
#     for num in numbers:
#         if num % 2 != 0:
#             total += num
#     return total

# print(sum_odd_nums([1, 2, 3, 4, 5]))

# def is_odd_num(num):
#     if num % 2 == 1:
#         return True
#     return False

# print(is_odd_num(5))

# def exponent(num=1, power=1):
#     return num**power

# print(exponent())

# def greet(name, greeting="Hi"):
#     return f"{greeting} {name}, how are you?"

# print(greet("John", "Hello"))

# num = 1

# def add(a, b):
#     return a + b

# hello = add

# print(hello(10, 2))

# print(add(10, 2))

# def add(a, b):
#     return a + b

# def sub(a, b):
#     return a - b

# math = [add, sub]

# print(math[1](10, 2))

# def add(a, b):
#     return a + b

# def sub(a, b):
#     return a - b

# def math(a, b, fn=add):
#     return fn(a, b)

# print(math(10, 5))

# def greet(name, message="hi"):
#     return f"{message}, {name}"

# print(greet(message="Hello", name="Jane"))

# name = "John Doe"

# def greet():
#     name = "Jane Smith"
#     print(name)

# greet()
# # print(name)

# total = 0

# def dummy():
#     global total
#     total += 5
#     print(total)

# dummy()
# print(total)

# PI = 3.14

# def outer():
#     total = 0

#     def inner():
#         nonlocal total
#         total += 5
#         print(total)

#     inner()

# outer()


def greet(name, message):
    """Function that greets a user
    @param name {str}
    @message message {str}
    @returns {str}
    """
    return f"{message}, {name}"


print(help(greet))

# print(greet("John", "Hello"))
# print(greet.__doc__)
# print(print.__doc__, end="\n")

# print("Hello World", end="")
# print("Goodbye World")
