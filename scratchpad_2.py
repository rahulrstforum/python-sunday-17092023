# # name = 'john'

# # # name = ['j', 'o', 'h', 'n']
# # # name = {0: 'j', 1: 'o', 2: 'h', 3: 'n'}
# # #

# name = "John Doe"

# # "JOHN DOE"
# name.find("Doe").upper()

# print("Enter your amount in INR")
# amount_in_inr = int(input())

# amount_in_inr = int(amount_in_inr)
# print(type(amount_in_inr))

# print("Enter your amount in USD")
# amount_in_usd = float(input("Enter your amount in USD: "))

# amount_in_inr = amount_in_usd * 82
# print(f"{amount_in_usd} USD is {amount_in_inr} INR")

# age = 15

# if age > 18:  # code block
#     print("Welcome")
#     print("Welcome")
#     print("Welcome")

# print("This is not inside the block")

# city = input("Please enter your city: ")

# if city == "Mumbai":
#     print("Hello")
#     print("Hello")
#     print("Hello")
#     print("Hello")
#     print("Hello")
# elif city == "Pune":
#     print("Hello again")
# elif city == "Delhi":
#     print("Goodbye")
# elif city == "Palghar":
#     print("Hello again")
# else:
#     print("Invalid city")

# age = int(input("Please enter your age: "))

# if age >= 65:
#     print("Drinks are free")
# elif age >= 21:
#     print("You can enter and you can drink")
# elif age >= 18:
#     print("You can enter but you can't drink")
# else:
#     print("You aren't allowed")

# logged_in_user = "john"

# # if logged_in_user == None:
# #     print("Sorry, you have to log in to view this content")
# # else:
# #     print("Here is the content")

# if logged_in_user:  # bool(logged_in_user)
#     print("Here is the content")
# else:
#     print("Sorry, you have to log in to view this content")

age = int(input("Please enter your age: "))

if age >= 18 and age < 21:
    print("You can enter but you can't drink")
elif age >= 21 and age < 65:
    print("You can enter and you can drink")
elif age >= 65:
    print("Drinks are free")
else:
    print("You aren't allowed")
