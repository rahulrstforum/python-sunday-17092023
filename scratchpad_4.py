# tasks = "my first task||my second task||"

# tasks = ['My first task', 'This is something else', 'hello', 'World']
# print(tasks[5])

# print("Hello World")

# langs = ["Python", "JavaScript", "Rust", "Elm", "WASM"]

# # for lang in langs:
# #     print(lang.upper())

# count = 0
# while count < len(langs):
#     print(langs[count])
#     count += 1

nums = [1, 2, 3, 4, 5]

doubles = [n * 2 for n in nums]  # [2, 4]

for num in nums:
    doubles.append(num * 2)

print(doubles)

names = ["John", "Jane", "Jack", "Jill"]

upper_names = []

for name in names:
    upper_names.append(name.upper())

print(upper_names)

upper_names2 = [name.upper() for name in names]
print(upper_names2)
