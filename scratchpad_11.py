# # # # def counter(start, end):
# # # #     while start < end:
# # # #         yield start
# # # #         start += 1


# # # # for num in counter(1, 11):
# # # #     print(num)


# # # def fib_list(max):
# # #     nums = []
# # #     a, b = 0, 1
# # #     while len(nums) < max:
# # #         nums.append(b)
# # #         a, b = b, a + b
# # #     return nums


# # # def fib_gen(max):
# # #     a, b = 0, 1
# # #     count = 0
# # #     while count < max:
# # #         a, b = b, a + b
# # #         yield a
# # #         count += 1


# # # # print(fib_list(100000000000000000000000))
# # # # print(fib_gen(100000000000000000000000))

# # # for num in fib_gen(100000000000000000000):
# # #     print(num)


# # def math(a, b, fn):
# #     return fn(a, b)


# # def add(a, b):
# #     return a + b


# # def sub(a, b):
# #     return a - b


# # print(math(10, 20, sub))


# # def sum(n, func):
# #     total = 0
# #     for num in range(1, n + 1):
# #         total += square(num)
# #     return total


# # def square(n):
# #     return n * n


# # def cube(n):
# #     return n * n * n


# # print(sum(3, square))

# import random


# # def greet(person):
# #     def get_mood():
# #         mood = ["Hey", "What!", "What the heck do you want!", "Get lost!"]
# #         msg = random.choice(mood)
# #         return msg

# #     result = f"{get_mood()} {person}"
# #     return result


# # print(greet("john"))


# # def make_greet(person):
# #     def make_message():
# #         msg = random.choice(["Hey", "What!", "What the heck do you want!", "Get lost!"])
# #         return f"{msg} {person}"

# #     return make_message


# # hello = make_greet("john")
# # print(hello)


# # def stars(fn):
# #     def wrapper():
# #         print("*" * 10)
# #         fn()
# #         print("*" * 10)

# #     return wrapper


# # # @stars
# # def greet():
# #     print("Hello World!")


# # # greet = stars(greet)

# # greet()


# # def make_upper_case(fn):
# #     def wrapper(*args, **kwargs):
# #         return fn(*args, **kwargs).upper()

# #     return wrapper


# # @make_upper_case
# # def greet():
# #     return "Hello World!"


# # @make_upper_case  # hello = make_upper_case(hello)
# # def hello(person):
# #     return f"Hello {person}"


# # @make_upper_case
# # def hello2(message, person):
# #     return f"{message}, {person}"


# # print(hello("John"))
# # print(hello2("Hi", "John"))


# from functools import wraps


def make_upper_case(fn):
    """Decorator that uppercases the return value"""

    @wraps(fn)
    def wrapper(*args, **kwargs):
        """Wrapper function"""
        return fn(*args, **kwargs).upper()

    return wrapper


# @make_upper_case
# def greet():
#     """Function that says hello"""
#     return "Hello World!"


# # print(make_upper_case.__doc__)
# print(greet.__doc__)
# print(greet.__name__)


# file = open("hello.txt")
# data = file.read()
# print(data)
# file.close()

# print(file.closed)

# with open("hello.txt") as file:
#     data = file.read()
#     print(data)

# print(file.closed)


# with open("hello.txt", "w") as file:
#     file.write("This is some test content\n")
