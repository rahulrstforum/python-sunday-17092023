# def add(a, b):
#     return a + b

# print(add(10, 5))


# def add(*nums):
#     total = 0
#     for num in nums:
#         total += num
#     return total


# print(add(10, 2, 2))

# def profile(**data):
#     print(data)

# profile(first_name="John", last_name="Doe", age=20)

# def dummy(a, b, c, *nums, role="administrator", **data):
#     print(a, b, c, nums, role, data)


# dummy(10, 20, 30, 40, 50, 60, 70, first_name="Jane", last_name="Doe", role="mod")


# def add(*nums):
#     total = 0
#     for num in nums:
#         total += num
#     return total

# data = [10, 20, 30]
# print(add(*data))


# def say_name(first, last):
#     print(f"My name is {first} {last}.")


# data = {"first": "Jane", "last": "Smith"}

# say_name(**data)

# def add(a, b):
#     return a + b

# a = 10
# hello = add

# print(hello(10, 2))


# def add(a, b):
#     return a + b

# sub = lambda a, b: a - b

# print(sub(10, 5))


# def math(a, b, fn):
#     return fn(a, b)

# # def add(a, b):
# #     return a + b

# # print(math(10, 20, add))
# print(math(10, 20, lambda a, b: a - b))

# math = [lambda a, b: a + b, lambda a, b: a - b, lambda a, b: a * b]

# print(math[1](10, 4))


# nums = [1,2,3,4,5]
# # doubles = [num * 2 for num in nums]
# doubles = map(lambda num: num * 2, nums)

# # doubles = []
# # for num in nums:
# #     doubles.append(num * 2)

# print(tuple(doubles))

# nums = [1,2,3,4,5]
# # evens = [num for num in nums if num % 2 == 0]
# evens = list(filter(lambda num: num % 2 == 0, nums))
# print(evens)


names = ['John', 'Jack', 'James', 'Desmond', 'Charlie', 'Jacob']

result = map(lambda name: name.upper(), filter(lambda name: len(name) <= 4, names))

print(list(result))
