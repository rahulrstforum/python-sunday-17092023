# # # import re

# # # # pattern = re.compile(r"\d{3}\s?\d{4}-?\d{4}")
# # # # result = pattern.search("Call us today at 022 2564-3245")

# # # # if result:
# # # #     print(result.group())


# # # # def extract_phone(query_str):
# # # #     phone_regex = re.compile(r"\d{3}\s?\d{4}-?\d{4}")
# # # #     match = phone_regex.search(query_str)
# # # #     if match:
# # # #         return match.group()
# # # #     return None


# # # # print(extract_phone("Call us today at 022 2564-3245 or 02223443245"))


# # # # def extract_all_phones(query_str):
# # # #     phone_regex = re.compile(r"\d{3}\s?\d{4}-?\d{4}")
# # # #     return phone_regex.findall(query_str)


# # # # print(extract_all_phones("Call us today at 022 2564-3245 or 02223443245"))


# # # # def is_valid_phone(phone_no):
# # # #     phone_regex = re.compile(r"^\d{3}\s?\d{4}-?\d{4}$")
# # # #     match = phone_regex.search(phone_no)
# # # #     if match:
# # # #         return True
# # # #     return False


# # # # def is_valid_phone(phone_no):
# # # #     phone_regex = re.compile(r"\d{3}\s?\d{4}-?\d{4}")
# # # #     match = phone_regex.fullmatch(phone_no)
# # # #     if match:
# # # #         return True
# # # #     return False


# # # # print(is_valid_phone("02223443245  "))


# # # # def check_pan_card(pan_card_no):
# # # #     ...


# # # class Validation:
# # #     @classmethod
# # #     def pan(cls, pan_card):
# # #         pan_regex = "LOGIC"
# # #         match = ""
# # #         if match:
# # #             return True
# # #         return False

# # #     @classmethod
# # #     def aadhaar(cls, aadhaar_card):
# # #         pass

# # #     @classmethod
# # #     def phone(cls, phone_no):
# # #         pass

# # #     @classmethod
# # #     def email(cls, email_address):
# # #         pass


# # # class Customer:
# # #     def __init__(self, first_name, last_name, pan_card):
# # #         if Validation.pan(pan_card):
# # #             self.pan_card = pan_card
# # #         else:
# # #             raise ValueError("Invalid Pan Card")

# # #         self.first_name = first_name
# # #         self.last_name = last_name

# # import threading
# # import multiprocessing
# # from datetime import datetime


# # def dummy_func(x):
# #     print(f"Job-{x} started: {datetime.now()}")
# #     a = []
# #     for i in range(40000):
# #         for j in range(2000):
# #             a.append([i, j])
# #             a.clear()
# #     print(f"Job-{x} ended: {datetime.now()}")


# # # start_time = datetime.now()
# # # dummy_func(1)
# # # dummy_func(2)
# # # dummy_func(3)
# # # dummy_func(4)
# # # print(f"Total time taken: {datetime.now() - start_time}")

# # if __name__ == "__main__":
# #     # t1 = threading.Thread(target=dummy_func, args=(1,))
# #     # t2 = threading.Thread(target=dummy_func, args=(2,))
# #     # t3 = threading.Thread(target=dummy_func, args=(3,))
# #     # t4 = threading.Thread(target=dummy_func, args=(4,))

# #     # start_time = datetime.now()
# #     # t1.start()
# #     # t2.start()
# #     # t3.start()
# #     # t4.start()
# #     # t1.join()
# #     # t2.join()
# #     # t3.join()
# #     # t4.join()
# #     # print(f"Total time taken: {datetime.now() - start_time}")

# #     p1 = multiprocessing.Process(target=dummy_func, args=(1,))
# #     p2 = multiprocessing.Process(target=dummy_func, args=(2,))
# #     p3 = multiprocessing.Process(target=dummy_func, args=(3,))
# #     p4 = multiprocessing.Process(target=dummy_func, args=(4,))

# #     start_time = datetime.now()
# #     p1.start()
# #     p2.start()
# #     p3.start()
# #     p4.start()
# #     p1.join()
# #     p2.join()
# #     p3.join()
# #     p4.join()
# #     print(f"Total time taken: {datetime.now() - start_time}")


# data = """
# <html>
# 	<head>
# 		<title>My Application</title>
# 		<meta charset="utf-8" />
# 	</head>
# 	<body>
# 		<div>
# 			<h1 class="hello">This is my app</h1>
# 			<p class="hello">
# 				Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illum, quod.
# 				Veritatis nesciunt temporibus perferendis ab aliquam pariatur, alias ad
# 				ullam nemo fugiat impedit ex, possimus dolorum debitis non deleniti
# 				dolore?
# 			</p>
# 			<section id="helloworld">
# 				<h4 class="hello">Some other title</h4>
# 			</section>
# 			<img src="./assets/test.png" alt="Some Description" />
# 		</div>
# 		<ul>
# 			<li>
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus,
# 				laudantium!
# 			</li>
# 			<li>
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus,
# 				laudantium!
# 			</li>
# 			<li>
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus,
# 				laudantium!
# 			</li>
# 			<li>
# 				Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus,
# 				laudantium!
# 			</li>
# 		</ul>
# 	</body>
# </html>
# """

# from bs4 import BeautifulSoup

# soup = BeautifulSoup(data, "html.parser")
# # print(soup.body.div.h1)

# # print(soup.find("li"))
# # print(soup.find_all("li"))

# # print(soup.find(id="helloworld"))
# # print(soup.find_all(class_="hello"))

# # print(soup.find("h1").get_text())
# # print(soup.find("img")["alt"])


import requests
from bs4 import BeautifulSoup
from csv import writer

response = requests.get("https://arstechnica.com/")

soup = BeautifulSoup(response.text, "html.parser")

articles = soup.find_all(class_="article")

with open("articles.csv", "w", newline="") as file:
    csv_writer = writer(file)
    csv_writer.writerow(["Title", "Excerpt", "Author", "Date", "URL"])

    for article in articles:
        title = article.find("h2").get_text()
        excerpt = article.find(class_="excerpt").get_text()
        author = article.find("span").get_text()
        date = article.find("time").get_text()
        url = article.find("a")["href"]
        csv_writer.writerow([title, excerpt, author, date, url])
