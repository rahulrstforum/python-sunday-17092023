# # class Math:
# #     @classmethod
# #     def add(cls, a, b):
# #         return a + b

# #     @classmethod
# #     def sub(cls, a, b):
# #         return a - b


# # print(Math.add(10, 330))


# # class User:
# #     country = "India"
# #     total = 0
# #     active = 0

# #     def __init__(self, first_name, last_name, age, email, password):
# #         self.first_name = first_name
# #         self.last_name = last_name
# #         self._age = age
# #         self.email = email
# #         self._password = password
# #         User.total += 1

# #     def __repr__(self):
# #         return f"Name: {self.first_name} {self.last_name}"

# #     def greet(self):
# #         return f"Hello, my name is {self.first_name} {self.last_name}."

# #     def get_age(self):
# #         return self._age

# #     def set_age(self, new_age):
# #         if new_age < 18 or new_age > 90:
# #             raise ValueError("Invalid age")
# #         self._age = new_age
# #         return self._age

# #     def login(self, password):
# #         if password != self._password:
# #             raise ValueError("Sorry, incorrect login creds.")
# #         User.active += 1
# #         return True

# #     def logout(self):
# #         User.active -= 1
# #         return True

# #     @classmethod
# #     def get_total_users(cls):
# #         return cls.total


# # user1 = User("John", "Doe", 20, "john@gmail.com", "12345")
# # user2 = User("Jane", "Smith", 20, "john@gmail.com", "12345")

# # print(user1)
# # print(user2)

# # print(user1.first_name)
# # print(user2.greet())
# # user1.set_age(25)
# # print(user1.get_age())

# # print(user1.country)
# # print(user2.country)
# # print(User.country)
# # user1.login("12345")
# # user2.login("12345")
# # user2.logout()
# # print(User.active)

# # print(User.get_total_users())


# # class User:
# #     def __init__(self, first_name, last_name, age, email, password):
# #         self.first_name = first_name
# #         self.last_name = last_name
# #         self._age = age
# #         self.email = email
# #         self._password = password

# #     def __repr__(self):
# #         return f"Name: {self.first_name} {self.last_name}"

# #     @property
# #     def age(self):
# #         return self._age

# #     @age.setter
# #     def age(self, new_age):
# #         if new_age < 18 or new_age > 90:
# #             raise ValueError("Invalid age")
# #         self._age = new_age
# #         return self._age

# #     def get_age(self):
# #         return self._age

# #     def set_age(self, new_age):
# #         if new_age < 18 or new_age > 90:
# #             raise ValueError("Invalid age")
# #         self._age = new_age
# #         return self._age


# # class Admin(User):
# #     def create_group(self, group_name):
# #         print(f"{group_name} was created.")

# # user1 = User("John", "Doe", 20, "john@gmail.com", "12345")
# # user1.age = 500
# # print(user1.age)

# # user1.set_age(40)
# # print(user1.get_age())

# # class Admin(User):
# #     def create_group(self, group_name):
# #         print(f"{group_name} was created.")


# # user1 = User("John", "Doe", 20, "john@gmail.com", "12345")
# # print(user1.greet())

# # user2 = Admin("Jane", "Doe", 22, "jane@outlook.com", "12131324")
# # print(user2.greet())
# # user2.create_group("Hello World")


# class User:
#     def __init__(self, first_name, last_name, age, email, password):
#         self.first_name = first_name
#         self.last_name = last_name
#         self._age = age
#         self.email = email
#         self._password = password

#     def __add__(self, another_user):
#         return User("New Born", self.last_name, 0, "", "")

#     def __repr__(self):
#         return f"Name: {self.first_name} {self.last_name}"

#     def __len__(self):
#         return self._age

#     def greet(self):
#         return "Hello World"

#     @property
#     def age(self):
#         return self._age

#     @age.setter
#     def age(self, new_age):
#         if new_age < 18 or new_age > 90:
#             raise ValueError("Invalid age")
#         self._age = new_age
#         return self._age


# class Admin(User):
#     def __init__(self, first_name, last_name, age, email, password, phone):
#         super().__init__(first_name, last_name, age, email, password)
#         self.phone = phone
#         self.country = "India"

#     def create_group(self, group_name):
#         print(f"{group_name} was created.")

#     def greet(self):
#         return f"Hello, my name is {self.first_name} {self.last_name}"


# user1 = User("John", "Doe", 20, "john@gmail.com", "12345")
# user2 = Admin("Jane", "Doe", 22, "jane@gmail.com", "123213", "+91 9876654321")

# # print(user1.greet())
# # print(user2.greet())

# # print(len("hello"))
# # print(len([1, 2, 3]))

# # print(len(user1))

# print(1 + 2)
# print("hello" + "world")
# print(user1 + user2)

# # class Human:
# #     def __init__(self, name):
# #         self.name = name

# #     def greet(self):
# #         print("Hello World")


# # class Animal:
# #     def __init__(self, name):
# #         self.name = name

# #     def greet(self):
# #         print("askdnkjasdnljasdnakjsld")


# # class Mutant(Animal, Human):
# #     pass


# # mut1 = Mutant("Logan")
# # mut1.greet()


# nums = [1, 2, 3, 4, 5]

# for num in nums:
#     print(num)

# for num = next(num) in iter(nums):
#     print(num)


# def my_for(iterable):
#     iterator = iter(iterable)

#     while True:
#         try:
#             print(next(iterator))
#         except StopIteration:
#             break


# nums = [1, 2, 3, 4, 5]
# my_for(nums)
# # for num in nums:
# #     print(num)


class Counter:
    def __init__(self, start, end, step=1):
        self.start = start
        self.end = end
        self.step = step

    def __repr__(self):
        return f"Counter({self.start}, {self.end})"

    def __iter__(self):
        return self

    def __next__(self):
        if self.start < self.end:
            num = self.start
            self.start += self.step
            return num
        else:
            raise StopIteration


r = range(0, 10, 2)
c = Counter(0, 10, 2)

# print(r)
# print(c)

for num in r:
    print(num)

for num in c:
    print(num)

# c = Counter(0, 10)
# for num = next(num) in iter(c):
#     print(num)
