# from tkinter import *
# from tkinter import ttk
# from tkinter import messagebox
# import sqlite3


# class UserManagementSystem:
#     def __init__(self, master):
#         self.master = master
#         self.master.title("User Management System")

#         self.name_label = Label(master, text="Name:")
#         self.name_label.grid(row=0, column=0, padx=5, pady=5)
#         self.name_entry = Entry(master, width=30)
#         self.name_entry.grid(row=0, column=1, padx=5, pady=5)

#         self.email_label = Label(master, text="Email:")
#         self.email_label.grid(row=1, column=0, padx=5, pady=5)
#         self.email_entry = Entry(master, width=30)
#         self.email_entry.grid(row=1, column=1, padx=5, pady=5)

#         self.phone_label = Label(master, text="Phone:")
#         self.phone_label.grid(row=2, column=0, padx=5, pady=5)
#         self.phone_entry = Entry(master, width=30)
#         self.phone_entry.grid(row=2, column=1, padx=5, pady=5)

#         self.add_button = Button(master, text="Add User", command=self.add_user)
#         self.add_button.grid(row=3, column=0, padx=5, pady=5)

#         self.update_button = Button(
#             master, text="Update User", command=self.update_user
#         )
#         self.update_button.grid(row=3, column=1, padx=5, pady=5)

#         self.delete_button = Button(
#             master, text="Delete User", command=self.delete_user
#         )
#         self.delete_button.grid(row=3, column=2, padx=5, pady=5)

#         self.table = ttk.Treeview(
#             master, columns=("id", "name", "email", "phone"), show="headings"
#         )
#         self.table.heading("id", text="ID")
#         self.table.heading("name", text="Name")
#         self.table.heading("email", text="Email")
#         self.table.heading("phone", text="Phone")
#         self.table.column("id", width=30)
#         self.table.column("name", width=150)
#         self.table.column("email", width=150)
#         self.table.column("phone", width=100)
#         self.table.grid(row=4, column=0, columnspan=3, padx=5, pady=5)

#         self.display_users()

#     def add_user(self):
#         name = self.name_entry.get()
#         email = self.email_entry.get()
#         phone = self.phone_entry.get()

#         if name == "" or email == "" or phone == "":
#             messagebox.showerror("Error", "Please enter all fields")
#         else:
#             conn = sqlite3.connect("users.db")
#             cursor = conn.cursor()
#             cursor.execute(
#                 "INSERT INTO users (name, email, phone) VALUES (?, ?, ?)",
#                 (name, email, phone),
#             )
#             conn.commit()
#             conn.close()

#             self.name_entry.delete(0, END)
#             self.email_entry.delete(0, END)
#             self.phone_entry.delete(0, END)

#             self.display_users()

#             messagebox.showinfo("Success", "User added successfully")

#     def update_user(self):
#         selected_user = self.table.focus()
#         if selected_user:
#             name = self.name_entry.get()
#             email = self.email_entry.get()
#             phone = self.phone_entry.get()

#             if name == "" or email == "" or phone == "":
#                 messagebox.showerror("Error", "Please enter all fields")
#             else:
#                 conn = sqlite3.connect("users.db")
#                 cursor = conn.cursor()

#                 user_id = self.table.item(selected_user)["values"][0]
#                 cursor.execute(
#                     "UPDATE users SET name=?, email=?, phone=? WHERE id=?",
#                     (name, email, phone, user_id),
#                 )
#                 conn.commit()
#                 conn.close()

#                 self.name_entry.delete(0, END)
#                 self.email_entry.delete(0, END)
#                 self.phone_entry.delete(0, END)

#                 self.display_users()
#                 messagebox.showinfo("Success", "User updated successfully")
#         else:
#             messagebox.showerror("Error", "Please select a user to update")

#     def delete_user(self):
#         selected_user = self.table.focus()
#         if selected_user:
#             confirmation = messagebox.askyesno(
#                 "Confirmation", "Are you sure you want to delete this user?"
#             )

#             if confirmation:
#                 conn = sqlite3.connect("users.db")
#                 cursor = conn.cursor()

#                 user_id = self.table.item(selected_user)["values"][0]

#                 cursor.execute("DELETE FROM users WHERE id=?", (user_id,))
#                 conn.commit()
#                 conn.close()

#                 self.display_users()
#                 messagebox.showinfo("Success", "User deleted successfully")
#         else:
#             messagebox.showerror("Error", "Please select a user to delete")

#     def display_users(self):
#         # Clear the table first
#         for record in self.table.get_children():
#             self.table.delete(record)

#         conn = sqlite3.connect("users.db")
#         cursor = conn.cursor()

#         cursor.execute("SELECT * FROM users")
#         rows = cursor.fetchall()

#         for row in rows:
#             self.table.insert("", END, values=row)

#         conn.close()


# root = Tk()
# user_management_system = UserManagementSystem(root)
# root.mainloop()
