# # # import hello
# # # hello.greet()

# # from hello import greet

# # greet()

# # print("scratchpad_9.py - The value of __name__ is: ", __name__)


# # def area(width, height):
# #     return width * height


# # width1 = 30
# # height1 = 40

# # rect1 = [30, 40]

# # rect1 = {"width": 30, "height": 40}

# # rect2 = {"width": 30, "height": 30}

# # # print(area(width1, height1))
# # # print(area(rect1[0], rect1[1]))
# # print(area(rect1["width"], rect1["height"]))


# # "hello".lower()

# # upperstr("")


# # class Rectangle:
# #     def __init__(self, width, height):
# #         self.width = width
# #         self.height = height

# #     def area(self):
# #         return self.width * self.height


# # def area(width, height):
# #     return width * height

# # rect1 = Rectangle(30, 40)

# # print(rect1.area())

# # def area(rectangle):
# #     return rectangle.width * rectangle.height


# # print(area(rect1))

# # rect1 = [30, 40]
# # rect1 = {"width": 30, "height": 40}
# # rect2 = {"width": 30, "height": 30}

# # # print(area(width1, height1))

# # print(type("hello"))


# def user(first_name, last_name, age, email, password):
#     def login(entered_password):
#         if entered_password == password:
#             return True
#         raise ValueError("Incorrect password")

#     return {
#         "first_name": first_name,
#         "last_name": last_name,
#         "age": age,
#         "email": email,
#         "password": password,
#         "country": "India",
#         "login": login,
#     }


# class User:
#     def __init__(self, first_name, last_name, age, email, password):
#         self.first_name = first_name
#         self.last_name = last_name
#         self.age = age
#         self.email = email
#         self.password = password
#         self.country = "India"

#     def login(self, entered_password):
#         if entered_password == self.password:
#             return True
#         raise ValueError("Incorrect password")


# john = user("John", "Doe", 20, "john@gmail.com", "12345")
# jane = User("Jane", "Smith", 23, "jane@gmail.com", "12345")

# print(type(john))
# print(type(jane))

# # print(john["first_name"])
# # print(jane.first_name)

# # john["email"] = "john@helloworld.com"
# # jane.email = "jane@hey.com"

# # print(john["email"])
# # print(jane.email)

# # print(john["login"]("12345aasdakjsdbjhasd"))
# # print(jane.login("1234asdjkasnbdjk5"))


# PI = 3.1415

# PI = "hello"
# print(PI)

# age = int(input("Enter your age: "))

# print(age)


# class Rectangle:
#     def __init__(self, width, height):
#         self.width = width
#         self.height = height

#     def area(self):
#         return self.width * self.height


# r1 = Rectangle(10, 20)
# r1.area()


# class Song:
#     def __init__(self, title, length, album, artist):
#         self.title = title
#         self.length = length
#         self.album = album
#         self.artist = artist


# song1 = Song("song #1", 2.45, "Some album", "John Doe")
# song2 = Song("Some other song", 1.47, "Hello World", "Jane Doe")

# print(song1.album)


class User:
    def __init__(self, first_name, last_name, age, email, password):
        self.first_name = first_name
        self.last_name = last_name
        self.__age = age
        self.email = email
        self.password = password

    def __len__(self):
        return self.__age

    def get_age(self):
        return self.__age

    def set_age(self, new_age):
        if new_age < 18 or new_age > 100:
            raise ValueError("Please enter a valid age")
        self.__age = new_age
        return True


john = User("John", "Doe", 20, "john@gmail.com", "123456")

print(len(john))

# john.email = "john.doe@hey.com"
# print(john.email)

# print(john._age)
# john.set_age(2500)

# print(john._User__age)
# john.__age = 999999999999999999999999999999
# print(john.get_age())
