# import random
# import pprint

# SUIT_TUPLE = ("Spades", "Hearts", "Clubs", "Diamonds")
# RANK_TUPLE = (
#     "Ace",
#     "2",
#     "3",
#     "4",
#     "5",
#     "6",
#     "7",
#     "8",
#     "9",
#     "10",
#     "Jack",
#     "Queen",
#     "King",
# )

# NCARDS = 8


# def get_card(deck_list_in):
#     this_card = deck_list_in.pop()
#     return this_card


# def shuffle(deck_list_in):
#     deck_list_out = deck_list_in.copy()
#     random.shuffle(deck_list_out)
#     return deck_list_out


# print("Welcome to higher or lower.")
# print(
#     "You have to choose whether the next card to be shown will be higher or lower than the current card."
# )
# print("Getting it right add 20 points; get it wrong and you will lose 15 points.")
# print("You have 50 points to start.")
# print()

# starting_deck_list = []
# for suit in SUIT_TUPLE:
#     for this_value, rank in enumerate(RANK_TUPLE):
#         card_dict = {"rank": rank, "suit": suit, "value": this_value + 1}
#         starting_deck_list.append(card_dict)

# score = 50


# while True:
#     game_deck_list = shuffle(starting_deck_list)
#     current_card_dict = get_card(game_deck_list)

#     current_card_rank = current_card_dict["rank"]
#     current_card_suit = current_card_dict["suit"]
#     current_card_value = current_card_dict["value"]

#     print(f"Starting card is {current_card_rank} of {current_card_suit}")
#     print()

#     for card_number in range(0, NCARDS):
#         answer = input(
#             f"Will the next card be higher or lower than the {current_card_rank} of {current_card_suit}? (enter h or l) "
#         )
#         answer = answer.lower()

#         next_card_dict = get_card(game_deck_list)
#         next_card_rank = next_card_dict["rank"]
#         next_card_suit = next_card_dict["suit"]
#         next_card_value = next_card_dict["value"]
#         print(f"Next card is {next_card_rank} of {next_card_suit}")

#         if answer == "h":
#             if next_card_value > current_card_value:
#                 print("You got it right, it was higher")
#                 score += 20
#             else:
#                 print("Sorry, it was not higher")
#                 score -= 15
#         elif answer == "l":
#             if next_card_value < current_card_value:
#                 print("You got it right, it was lower")
#                 score += 20
#             else:
#                 print("Sorry it was not lower")
#                 score -= 15

#         print(f"Your score is: {score}")
#         print()
#         current_card_rank = next_card_rank
#         current_card_suit = next_card_suit
#         current_card_value = next_card_value

#     go_again = input("To play again, press ENTER, or 'q' to quit: ")
#     if go_again == "q":
#         break

# print("Thank you for playing")


def greet(name, message, color):
    colors = ("red", "yellow", "green", "blue", "purple")

    if type(name) != str:
        raise TypeError("Name has to be a string")
    if type(message) != str:
        raise TypeError("Message has to a string")
    if type(color) != str:
        raise TypeError("Color has to a string")
    if color not in colors:
        raise ValueError("Invalid color")

    return f"{message}, {name} - in color {color}"


# greet("John Doe", "Hello", "maroon")

# try:
#     result = greet("John Doe", 10, "yellow")
#     print(result)
# except:
#     print("Something went wrong")
# except ValueError:
#     print("Something went wrong")
# except (ValueError, TypeError):
#     print("Something went wrong")
# except TypeError as err:
#     print(err)
# except (ValueError, TypeError) as err:
#     print(err)
# except Exception as err:
#     print(err)


# try:
#     result = greet("John Doe", "Hi", "yellow")
# except Exception as err:
#     print(err)
# else:
#     print(result)
# finally:
#     print("This will always run!")

# print("Program completed")


# import getpass


# password = getpass.getpass()
# print(password)

# import random as r
from random import randint, random, shuffle, choice

# from random import *


# def random():
#     print("Something")


random()
print(random())
print(randint(10, 100))
