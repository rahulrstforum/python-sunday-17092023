# # The line below prints some text

# print("Hello World")  # SOme content

# # this is my comment
# # and this is another comment

# a = 10
# b = 5

# score = a + b
# print(score)

# score = 10

# avg_score = 10

player_one_high_score = 100  # snake casing
playerOneHighScore = 100  # camel casing
PlayerOneHighScore = 100  # Pascal casing
# player-one-high-score = 100 # kebab casing
