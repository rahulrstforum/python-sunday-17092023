# def greet():
#     print("Hello World")


# print("This is present in the hello.py file")

# print("hello.py - The value of __name__ is: ", __name__)


def greet():
    print("Hello World")


def add(a, b):
    return a + b


def main():
    print("This is present in the hello.py file")


if __name__ == "__main__":
    main()
